# Flowery Field

A small minesweeper clone made using Go and the [Raylib graphics library](https://www.raylib.com/).

Includes a toggleable dark/light mode.

## Screenshots

TBD

## Dependencies

The only dependency is the raylib bindings for go, which can be installed like this:
```console
$ go get github.com/gen2brain/raylib-go/raylib
```

## Compiling & Running

```console
$ go build ./game.go
$ ./game
```

## Note for Windows users

On Windows a raylib.dll might be need, there are some machines where the game would crash before opening the window.

You can grab one from here: <https://github.com/raysan5/raylib/releases/download/5.0/raylib-5.0_win64_mingw-w64.zip>

Copy it from the `lib\` directory of the archive to the directory where the game executable is located.
