package main

import (
	"fmt"
	"os"
	"math/rand"
	"strconv"

	raylib "github.com/gen2brain/raylib-go/raylib"
)

const (
	FIELD_ROWS = 8;
	FIELD_COLUMNS = 15;

	CELL_FONT_SIZE = 40;
	CELL_PADDING = 10;

	GAME_INFO_PADDING_BOTTOM = 20;
	GAME_INFO_FONT_SIZE = 30;

	BUTTON_PADDING = 10;
	BUTTON_OUTLINE = 5;
	BUTTON_DESC_FONT_SIZE = 30;

	OVERLAY_FONT_SIZE = 60;
)

var (
	DM_COLOR_BACKGROUND = raylib.Color{ R: 0x11, G: 0x11, B: 0x11, A: 0xff }
	DM_COLOR_OVERLAY    = raylib.Color{ R: 0xee, G: 0xee, B: 0xee, A: 0xff }
	DM_COLOR_COUNTER_BG = raylib.Color{ R: 0x33, G: 0x33, B: 0x33, A: 0xff }
	DM_COLOR_MINE_BG    = raylib.Color{ R: 0xee, G: 0x11, B: 0x11, A: 0xff }
	DM_COLOR_GAME_INFO  = raylib.Color{ R: 0x33, G: 0x33, B: 0x33, A: 0xff }
	DM_COLOR_TOGGLE_THEME_SELECTED = raylib.Color{ R: 0xbb, G: 0xaa, B: 0x11, A: 0xff }
	DM_COLOR_TOGGLE_THEME_UNSELECTED = raylib.Color{ R: 0x77, G: 0x55, B: 0x11, A: 0xff }

	LM_COLOR_BACKGROUND = raylib.Color{ R: 0xdd, G: 0xdd, B: 0xdd, A: 0xff }
	LM_COLOR_OVERLAY    = raylib.Color{ R: 0x11, G: 0x11, B: 0x11, A: 0xff }
	LM_COLOR_COUNTER_BG = raylib.Color{ R: 0xaa, G: 0xaa, B: 0xaa, A: 0xff }
	LM_COLOR_MINE_BG    = raylib.Color{ R: 0xee, G: 0x11, B: 0x11, A: 0xff }
	LM_COLOR_GAME_INFO  = raylib.Color{ R: 0xaa, G: 0xaa, B: 0xaa, A: 0xff }
	LM_COLOR_TOGGLE_THEME_SELECTED = raylib.Color{ R: 0x11, G: 0x11, B: 0x88, A: 0xff }
	LM_COLOR_TOGGLE_THEME_UNSELECTED = raylib.Color{ R: 0x11, G: 0x11, B: 0x55, A: 0xff }
)

var (
	DARK_MODE = true;

	COLOR_BACKGROUND  = DM_COLOR_BACKGROUND;
	COLOR_OVERLAY     = DM_COLOR_OVERLAY;

	COLOR_HIDDEN_CELL_SELECTED   = raylib.LightGray;
	COLOR_HIDDEN_CELL_UNSELECTED = raylib.Gray;

	COLOR_COUNTER_BG  = DM_COLOR_COUNTER_BG;
	COLOR_MINE_BG     = DM_COLOR_MINE_BG;
	COLOR_GAME_INFO   = DM_COLOR_GAME_INFO;
	COLOR_TOGGLE_THEME_UNSELECTED = DM_COLOR_TOGGLE_THEME_UNSELECTED;
	COLOR_TOGGLE_THEME_SELECTED   = DM_COLOR_TOGGLE_THEME_SELECTED;
	COLOR_RESTART_UNSELECTED = COLOR_HIDDEN_CELL_UNSELECTED;
	COLOR_RESTART_SELECTED   = COLOR_HIDDEN_CELL_SELECTED;

	COLOR_COUNTER_FG = [10]raylib.Color{
		1: raylib.Blue,
		2: raylib.Green,
		3: raylib.Red,
		4: raylib.DarkBlue,
		5: raylib.Maroon,
		6: raylib.SkyBlue,
		7: raylib.Black,
		8: raylib.DarkGray,
	}
)

type Cell_Type int;
const (
	Mine Cell_Type = iota
	Counter
)

type Cell struct {
	ctype   Cell_Type
	counter uint
}

func (cell Cell) IsMine() bool {
	return cell.ctype == Mine;
}
func (cell Cell) IsEmpty() bool {
	return cell.ctype == Counter && cell.counter == 0;
}

func (cell Cell) Print() {
	switch cell.ctype {
	case Mine:
		fmt.Print("*");
	case Counter:
		{
			if cell.counter == 0 {
				fmt.Print(".");
			} else {
				fmt.Print(cell.counter);
			}
		}
	}
}

type Field [FIELD_ROWS][FIELD_COLUMNS]Cell;

func (field *Field) Reset() {
	for y, row := range field {
		for x, _ := range row {
			field[y][x] = Cell{ctype: Counter, counter: 0};
		}
	}
}

func (field *Field) Print() {
	for _, row := range field {
		for _, col := range row {
			col.Print();
			fmt.Print(" ");
		}
		fmt.Println();
	}
}

func (field *Field) PlaceMine(y, x int) {
	Assert(y >= 0 && y < FIELD_ROWS && x >= 0 && x < FIELD_COLUMNS,
		fmt.Sprint("Can't place mine at (y = ", y, ", x = ", x, "), out of bounds"));

	field[y][x] = Cell{Mine, 0};

	for dy := -1; dy <= 1; dy++ {
		if y + dy < 0 || y + dy >= FIELD_ROWS {
			continue;
		}

		for dx := -1; dx <= 1; dx++ {
			if x + dx < 0 || x + dx >= FIELD_COLUMNS {
				continue;
			}

			cell := &field[y+dy][x+dx];
			if cell.IsMine() {
				continue;
			}
			cell.counter += 1;
		}
	}
}

func (field *Field) RandomBombs(num int) {
	for num > 0 {
		for y, row := range field {
			for x, cell := range row {
				if RandomBoolean(20.0) && !cell.IsMine() {
					field.PlaceMine(y, x);
					num--;
				}
				if num == 0 { return; }
			}
		}
	}
}

type ViewportCell uint;
const (
	Hidden ViewportCell = iota
	Uncovered
	Flagged
)

type Viewport [FIELD_ROWS][FIELD_COLUMNS]ViewportCell;

func (viewport *Viewport) Reset() {
	for y, row := range viewport {
		for x, _ := range row {
			viewport[y][x] = Hidden;
		}
	}
}

type GameStatus uint;
const (
	GameOver GameStatus = iota
	Playing
	GameWon
)

type Game struct {
	field    Field
	viewport Viewport

	IsFirstMove bool
	bombs_num int
	flags_num int

	status GameStatus
}

func (game *Game) Reset() {
	game.field.Reset();
	game.viewport.Reset();

	game.IsFirstMove = true;
	game.bombs_num = 0;
	game.flags_num = 0;
	game.status = Playing;
}

func (game *Game) Print() {
	for y, row := range game.viewport {
		for x, view_cell := range row {
			switch view_cell {
				case Hidden: fmt.Print("#");
				case Uncovered: game.field[y][x].Print();
				case Flagged: fmt.Print("F");
			}
			fmt.Print(" ");
		}
		fmt.Println();
	}
}

func (game *Game) Regenerate(bombs_num int) {
	game.field.RandomBombs(bombs_num);
	game.bombs_num = bombs_num;
}

func MyDrawText(text string, position raylib.Vector2, size float32, color raylib.Color, font raylib.Font) {
	raylib.DrawTextEx(font, text, position, size, 2, color);
}

func MyMeasureText(text string, size float32, font raylib.Font) raylib.Vector2 {
	return raylib.MeasureTextEx(font, text, size, 2);
}
func (cell Cell) Render(rect raylib.Rectangle, font raylib.Font) {
	bg_color := COLOR_COUNTER_BG;
	if cell.ctype == Mine {
		bg_color = COLOR_MINE_BG;
	}
	raylib.DrawRectangleRec(rect, bg_color);

	if cell.ctype == Counter && cell.counter > 0 {
		text  := strconv.Itoa(int(cell.counter));
		size  := MyMeasureText(text, CELL_FONT_SIZE, font);
		text_position := raylib.Vector2{
			rect.X + rect.Width / 2 - size.X / 2,
			rect.Y + rect.Height / 2 - size.Y / 2,
		};

		Assert(cell.counter < 9, "Cell counter can't be bigger then 8");
		color := COLOR_COUNTER_FG[cell.counter];

		MyDrawText(text, text_position, CELL_FONT_SIZE, color, font);
	}
}

func (game *Game) CheckWinCondition() {
	uncovered_cells := 0;
	for _, row := range game.viewport {
		for _, cell := range row {
			if cell == Uncovered {
				uncovered_cells++;
			}
		}
	}

	if FIELD_ROWS * FIELD_COLUMNS - game.bombs_num == uncovered_cells {
		game.status = GameWon;
	}
}

func (game *Game) HandleFrame(cell_size float32, font raylib.Font, camera raylib.Camera2D) {
	mouse_world_position := raylib.GetScreenToWorld2D(raylib.GetMousePosition(), camera);
	for y, row := range game.field {
		for x, cell := range row {
			render_rect := raylib.Rectangle{
				X: float32(x) * (cell_size + CELL_PADDING),
				Y: float32(y) * (cell_size + CELL_PADDING),
				Width: cell_size,
				Height: cell_size,
			};

			switch game.viewport[y][x] {
			case Uncovered:
				cell.Render(render_rect, font);
			case Hidden:
				render_color := COLOR_HIDDEN_CELL_UNSELECTED;

				if game.status == Playing && raylib.CheckCollisionPointRec(mouse_world_position, render_rect) {
					render_color = COLOR_HIDDEN_CELL_SELECTED;
					if raylib.IsMouseButtonReleased(raylib.MouseButtonLeft) {
						if game.IsFirstMove {
							game.IsFirstMove = false;
							for !game.field[y][x].IsEmpty() {
								game.field.Reset();
								game.Regenerate(game.bombs_num);
							}
						}
						game.UncoverCellAt(y, x);
						game.CheckWinCondition();
					} else if raylib.IsMouseButtonReleased(raylib.MouseButtonRight) {
						game.ToggleFlagAt(y, x);
					}
				}
				raylib.DrawRectangleRec(render_rect, render_color);
			case Flagged:
				render_color := COLOR_HIDDEN_CELL_UNSELECTED;
				mouse_world_position := raylib.GetScreenToWorld2D(raylib.GetMousePosition(), camera);
				if game.status == Playing && raylib.CheckCollisionPointRec(mouse_world_position, render_rect) {
					render_color = COLOR_HIDDEN_CELL_SELECTED;
					if raylib.IsMouseButtonReleased(raylib.MouseButtonRight) {
						game.ToggleFlagAt(y, x);
					}
				}
				raylib.DrawRectangleRec(render_rect, render_color);
				DrawFlag(raylib.Vector2{render_rect.X, render_rect.Y}, render_rect.Width);
			}
		}
	}
}

func (game *Game) UncoverCellAt(y, x int) {
	game.viewport[y][x] = Uncovered;

	if game.field[y][x].IsEmpty() {
		for dy := -1; dy <= 1; dy++ {
			ay := y + dy;
			if ay < 0 || ay >= FIELD_ROWS {
				continue;
			}
			for dx := -1; dx <= 1; dx++ {
				ax := x + dx;
				if ax < 0 || ax >= FIELD_COLUMNS {
					continue;
				}
				if game.viewport[ay][ax] == Hidden {
					game.UncoverCellAt(ay, ax);
				}
			}
		}
	} else if game.field[y][x].IsMine() {
		game.status = GameOver;
	}
}

func (game *Game) ToggleFlagAt(y, x int) {
	switch game.viewport[y][x] {
		case Flagged:
			game.viewport[y][x] = Hidden;
			game.flags_num--;
		case Hidden:
			game.viewport[y][x] = Flagged;
			game.flags_num++;
		default: Assert(false, "unreachable");
	}
	game.CheckWinCondition();
}

func InsetRectangle(rect raylib.Rectangle, value float32) raylib.Rectangle {
	return raylib.Rectangle{
		rect.X + value,
		rect.Y + value,
		rect.Width - 2 * value,
		rect.Height - 2 * value,
	};
}

func ToggleThemeButton(camera raylib.Camera2D, rect raylib.Rectangle) {
	raylib.DrawRectangleLinesEx(rect, BUTTON_OUTLINE, COLOR_TOGGLE_THEME_SELECTED);
	rect = InsetRectangle(rect, BUTTON_OUTLINE);

	render_color := COLOR_TOGGLE_THEME_UNSELECTED;
	mouse_world_position := raylib.GetScreenToWorld2D(raylib.GetMousePosition(), camera);
	if raylib.CheckCollisionPointRec(mouse_world_position, rect) {
		render_color = COLOR_TOGGLE_THEME_SELECTED;
		if raylib.IsMouseButtonReleased(raylib.MouseButtonLeft) {
			if DARK_MODE {
				COLOR_BACKGROUND = LM_COLOR_BACKGROUND;
				COLOR_OVERLAY    = LM_COLOR_OVERLAY;
				COLOR_COUNTER_BG = LM_COLOR_COUNTER_BG;
				COLOR_MINE_BG    = LM_COLOR_MINE_BG;
				COLOR_GAME_INFO  = LM_COLOR_GAME_INFO;
				COLOR_TOGGLE_THEME_UNSELECTED = LM_COLOR_TOGGLE_THEME_UNSELECTED;
				COLOR_TOGGLE_THEME_SELECTED   = LM_COLOR_TOGGLE_THEME_SELECTED;
			} else {
				COLOR_BACKGROUND = DM_COLOR_BACKGROUND;
				COLOR_OVERLAY    = DM_COLOR_OVERLAY;
				COLOR_COUNTER_BG = DM_COLOR_COUNTER_BG;
				COLOR_MINE_BG    = DM_COLOR_MINE_BG;
				COLOR_GAME_INFO  = DM_COLOR_GAME_INFO;
				COLOR_TOGGLE_THEME_UNSELECTED = DM_COLOR_TOGGLE_THEME_UNSELECTED;
				COLOR_TOGGLE_THEME_SELECTED   = DM_COLOR_TOGGLE_THEME_SELECTED;
			}
			DARK_MODE = !DARK_MODE;
		}
	}
	raylib.DrawRectangleRec(rect, render_color);
}

func RestartButton(camera raylib.Camera2D, rect raylib.Rectangle, game *Game) {
	raylib.DrawRectangleLinesEx(rect, BUTTON_OUTLINE, COLOR_RESTART_SELECTED);
	rect = InsetRectangle(rect, BUTTON_OUTLINE);

	render_color := COLOR_RESTART_UNSELECTED;
	mouse_world_position := raylib.GetScreenToWorld2D(raylib.GetMousePosition(), camera);
	if raylib.CheckCollisionPointRec(mouse_world_position, rect) {
		render_color = COLOR_RESTART_SELECTED;
		if raylib.IsMouseButtonReleased(raylib.MouseButtonLeft) {
			bombs_num := game.bombs_num;
			game.Reset();
			game.Regenerate(bombs_num);
		}
	}
	raylib.DrawRectangleRec(rect, render_color);
}

func (game Game) GameInfoLabel(origin raylib.Vector2, window_size raylib.Vector2, font raylib.Font) {
	text := fmt.Sprintf("size: %dx%d   (%d bombs)", FIELD_COLUMNS, FIELD_ROWS, game.bombs_num);
	text_size := MyMeasureText(text, GAME_INFO_FONT_SIZE, font);

	coords := raylib.Vector2{
		origin.X + (window_size.X - text_size.X) / 2,
		origin.Y + window_size.Y - text_size.Y - GAME_INFO_PADDING_BOTTOM,
	};
	MyDrawText(text, coords, GAME_INFO_FONT_SIZE, COLOR_GAME_INFO, font);
}

func (game *Game) RenderUI(camera raylib.Camera2D, window_size raylib.Vector2, font raylib.Font) {
	origin := raylib.GetScreenToWorld2D(raylib.Vector2{}, camera);

	if game.status != Playing { // overlay
		text := "";
		switch game.status {
			case GameOver: text = "Game Over!";
			case GameWon:  text = "You won!";
			default: Assert(false, "unreachable");
		}
		text_render_width := MyMeasureText(text, OVERLAY_FONT_SIZE, font).X;

		MyDrawText(text, raylib.Vector2{origin.X + (window_size.X - text_render_width) / 2, origin.Y + window_size.Y / 2}, OVERLAY_FONT_SIZE, COLOR_OVERLAY, font);
	}

	// game information
	game.GameInfoLabel(origin, window_size, font);

	{ // buttons
		button_height := MyMeasureText("Foo bar", BUTTON_DESC_FONT_SIZE, font).Y;
		restart_rect := raylib.Rectangle{
			origin.X + BUTTON_PADDING,
			origin.Y + BUTTON_PADDING,
			button_height, button_height,
		}
		RestartButton(camera, restart_rect, game);
		MyDrawText("Restart", raylib.Vector2{restart_rect.X + restart_rect.Width + BUTTON_PADDING, restart_rect.Y}, BUTTON_DESC_FONT_SIZE, COLOR_RESTART_UNSELECTED, font);

		theme_rect := raylib.Rectangle{
			restart_rect.X,
			restart_rect.Y + restart_rect.Height + BUTTON_PADDING,
			button_height, button_height,
		};
		ToggleThemeButton(camera, theme_rect);
		MyDrawText("Toggle Dark/Light Mode", raylib.Vector2{theme_rect.X + theme_rect.Width + BUTTON_PADDING, theme_rect.Y}, BUTTON_DESC_FONT_SIZE, COLOR_TOGGLE_THEME_UNSELECTED, font);
	}
}

func RandomBoolean(percentage float32) bool {
	switch {
		case percentage <= 0.0:   return false;
		case percentage >= 100.0: return true;
		default: return rand.Float32() * 100.0 <= percentage;
	}
}

func RandomInt(min, max int) int {
	if min > max {
		min, max = max, min;
	}
	return min + rand.Int() % (max - min);
}

func Assert(cond bool, message string) {
	if !cond {
		fmt.Print("Assertion failed: \"", message, "\"\n");
		os.Exit(1);
	}
}

func DrawFlag(pos raylib.Vector2, size float32) {
	// pole
	raylib.DrawRectangleV(raylib.Vector2{pos.X + 0.2 * size, pos.Y + 0.2 * size}, raylib.Vector2{0.2 * size, 0.6 * size}, raylib.Red);
	// flag
	raylib.DrawTriangle(
		raylib.Vector2{pos.X + 0.4 * size, pos.Y + 0.2 * size},
		raylib.Vector2{pos.X + 0.4 * size, pos.Y + 0.7 * size},
		raylib.Vector2{pos.X + 0.8 * size, pos.Y + 0.45 * size}, raylib.Red);
}

func GetRenderSize() raylib.Vector2 {
	return raylib.Vector2{
		float32(raylib.GetRenderWidth()), float32(raylib.GetRenderHeight()),
	};
}

func main() {
	game := Game{};
	game.Reset();
	game.Regenerate(20);

	raylib.InitWindow(1280, 720, "Flowery Field");
	raylib.SetTextLineSpacing(GAME_INFO_FONT_SIZE);

	font := raylib.LoadFontEx("fonts/vermin_vibes_1989/Vermin Vibes 1989.ttf", 80, nil, 0);

	text_size := MyMeasureText("9", CELL_FONT_SIZE, font);
	cell_size := max(text_size.X, text_size.Y);
	text_padding := cell_size / 12;
	cell_size += text_padding * 2;

	field_render_width := (cell_size + CELL_PADDING) * FIELD_COLUMNS - CELL_PADDING;
	field_render_height := (cell_size + CELL_PADDING) * FIELD_ROWS - CELL_PADDING;

	win_size := GetRenderSize();
	win_width, win_height := win_size.X, win_size.Y;
	window_horiz_padding := (win_width - field_render_width) / 2;
	window_vert_padding :=  (win_height - field_render_height) / 2;

	camera := raylib.Camera2D{
		Target: raylib.Vector2{
			X: -window_horiz_padding,
			Y: -window_vert_padding,
		},
		Zoom: 1.0,
	};

	for !raylib.WindowShouldClose() {
		win_size = GetRenderSize();
		win_width, win_height = win_size.X, win_size.Y;

		raylib.BeginDrawing();
		raylib.BeginMode2D(camera);

		raylib.ClearBackground(COLOR_BACKGROUND);

		game.HandleFrame(cell_size, font, camera);
		game.RenderUI(camera, win_size, font);

		raylib.EndMode2D();
		raylib.EndDrawing();
	}

	raylib.UnloadFont(font);

	raylib.CloseWindow();
}
