module flowery-field

go 1.21.5

require github.com/gen2brain/raylib-go/raylib v0.0.0-20240507090038-2a66186c7de0

require (
	github.com/ebitengine/purego v0.7.1 // indirect
	golang.org/x/sys v0.20.0 // indirect
)
